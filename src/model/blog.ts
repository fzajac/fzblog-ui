import { Category } from "./category";

export class Blog {

  id: number;
  name: string;
  tagline?: string;
  about?: string;
  key: string;
  icon?: string;

  constructor(dto: any) {
    this.id = dto.id;
    this.name = dto.name;
    if (dto.tagline) {
      this.tagline = dto.tagline;
    }
    if (dto.about) {
      this.about = dto.about;
    }
    this.key = dto.key;
    if (dto.icon) {
      this.icon = dto.icon;
    }
  }

}
