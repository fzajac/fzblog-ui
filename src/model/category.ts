export class Category {

  id: number;
  name: string;

  constructor(dto: any) {
    this.id = dto.id;
    this.name = dto.name;
  }

}
