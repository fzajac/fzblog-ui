import { Category } from "./category";

export class Post {

  id: number;
  authorId: number;
  authorDisplayName: string;
  created: Date;
  updated?: Date;
  published: boolean;
  title: string;
  tags: string;
  views: number;
  synopsis: string;
  body?: string;
  categories?: Array<Category>;
  coverImageURL: string;

  constructor(dto: any) {
    this.id = dto.id;
    this.authorId = dto.authorId;
    this.authorDisplayName = dto.authorDisplayName;
    this.created = new Date(dto.created);
    if (dto.updated) {
      this.updated = new Date(dto.updated);
    }
    this.published = dto.published;
    this.title = dto.title;
    this.tags = dto.tags;
    this.views = dto.views;
    this.synopsis = dto.synopsis;
    if (dto.body) {
      this.body = dto.body;
    }
    if (dto.categories) {
      this.categories = dto.categories;
    }
    this.coverImageURL = dto.coverImageURL;
  }

}
