import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['../app.component.scss', './about.component.scss']
})
export class AboutComponent implements OnInit {

  name?: string;

  about?: string;

  ngOnInit(): void {
    this.name = history.state.name;
    this.about = history.state.about;
  }

}
