import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Category } from 'src/model/category';
import { Post } from 'src/model/post';
import { CategoryService } from '../service/category.service';
import { PostService } from '../service/post.service';
import { AbstractPostsPage } from '../post-list/abstract-posts-page';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['../app.component.scss', './categories.component.scss']
})
export class CategoriesComponent extends AbstractPostsPage implements OnInit {

  categories: Array<Category> = [];

  categoryId: number = -1;

  constructor(
    private categoryService: CategoryService,
    private postService: PostService,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this.loading = true;
    this.categoryService.getAll().subscribe(response => {
      response.map(category => {
        this.categories.push(new Category(category))
      });
      this.loading = false;
      this.route.params.subscribe(
        params => {
          const id = +params['id'];
          if (id) {
            this.loadPage(id);
          } else {
            this.allPostsLoaded = true;
          }
        }
      );
    });
  }

  override loadPage(categoryId?: number): void {
    this.loading = true;
    if (categoryId) {
      this.page = 0;
      this.allPostsLoaded = false;
      this.posts = [];
      this.categoryId = categoryId;
    }
    this.postService.getAllByCategory(this.categoryId, this.page, this.size).subscribe(response => {
      response.map(post => {
        this.posts.push(new Post(post))
      });
      if (response.length < this.size) {
        this.allPostsLoaded = true;
      }
      this.loading = false;
    });
  }

}
