import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";

import { Blog } from 'src/model/blog';
import { BlogService } from './service/blog.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  blog: Blog | null | undefined;

  loading: boolean = true;

  constructor(
    private blogService: BlogService,
    private titleService: Title
  ) { }

  ngOnInit() {
    this.blogService.get().subscribe(response => {
      this.blog = new Blog(response);
      let title = this.blog.name;
      if (this.blog.tagline) {
        title += " • " + this.blog.tagline;
      }
      this.titleService.setTitle(title);
      this.loading = false;
    });
  }

}
