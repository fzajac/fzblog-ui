import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Post } from 'src/model/post';
import { PostService } from '../service/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['../app.component.scss', './post.component.scss']
})
export class PostComponent implements OnInit {

  loading: boolean = true;

  post: Post | null | undefined;

  constructor(
    private postService: PostService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const postId = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    this.postService.get(postId).subscribe(response => {
      this.post = new Post(response);
      this.loading = false;
    });
  }

}
