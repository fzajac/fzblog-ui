import { Component, Input } from '@angular/core';
import { DatePipe } from '@angular/common';

import { Post } from 'src/model/post';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent {

  @Input({ required: true })
  post!: Post;

  @Input({ required: false })
  fullView: boolean = false;

  constructor(
    private datepipe: DatePipe
  ) { }

  getPostDateTooltip(post: Post): string {
    let details = "First published: " + this.datepipe.transform(post.created, "yyyy-MM-dd HH:mm:ss");
    if (post.updated) {
      details += "\nLast updated: " + this.datepipe.transform(post.updated, "yyyy-MM-dd HH:mm:ss");
    }
    return details;
  }

}
