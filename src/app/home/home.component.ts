import { Component, OnInit } from '@angular/core';

import { Post } from 'src/model/post';
import { PostService } from '../service/post.service';
import { AbstractPostsPage } from '../post-list/abstract-posts-page';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['../app.component.scss', './home.component.scss']
})
export class HomeComponent extends AbstractPostsPage implements OnInit {

  constructor(
    private postService: PostService
  ) {
    super();
  }

  ngOnInit() {
    this.loadPage();
  }

  override loadPage(): void {
    this.loading = true;
    this.postService.getAll(this.page, this.size).subscribe(response => {
      response.map(post => {
        this.posts.push(new Post(post))
      });
      if (response.length < this.size) {
        this.allPostsLoaded = true;
      }
      this.loading = false;
    });
  }

}
