import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ServiceHelper } from './service-helper';
import { Category } from 'src/model/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Category[]> {
    const url = ServiceHelper.resolveAPIURL() + '/categories';
    return this.http.get<Category[]>(url);
  }

}
