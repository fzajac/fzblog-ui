import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ServiceHelper } from './service-helper';
import { Blog } from 'src/model/blog';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) { }

  public get(): Observable<Blog> {
    const url = ServiceHelper.resolveAPIURL();
    return this.http.get<Blog>(url);
  }

}
