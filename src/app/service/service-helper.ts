import { isDevMode } from '@angular/core';
import { environment } from 'src/environments/environment';

export class ServiceHelper {

  static resolveAPIURL(): string {
    let url = "";
    if (isDevMode()) {
      url += environment.devURL;
    }
    return url + "/api/blog/" + environment.blogKey;
  }

}
