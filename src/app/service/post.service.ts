import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ServiceHelper } from './service-helper';
import { Post } from 'src/model/post'

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  public getAll(page: number, size: number): Observable<Post[]> {
    const url = ServiceHelper.resolveAPIURL()
      + '/posts?page=' + page + "&size=" + size;
    return this.http.get<Post[]>(url);
  }

  public getAllByCategory(categoryId: number, page: number, size: number): Observable<Post[]> {
    const url = ServiceHelper.resolveAPIURL()
      + '/posts/category/' + categoryId + '?page=' + page + "&size=" + size;
    return this.http.get<Post[]>(url);
  }

  public get(id: number): Observable<Post> {
    const url = ServiceHelper.resolveAPIURL()
      + '/posts/' + id;
    return this.http.get<Post>(url);
  }

}
