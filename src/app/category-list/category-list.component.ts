import { Component, Input } from '@angular/core';
import { Category } from 'src/model/category';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent {

  @Input({ required: true })
  categories!: Array<Category>;

  @Input({ required: false })
  selectedId: number = -1;

}
