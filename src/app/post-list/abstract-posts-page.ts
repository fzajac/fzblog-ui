import { Post } from "src/model/post";

export abstract class AbstractPostsPage {

  posts: Array<Post> = [];

  page: number = 0;

  size: number = 10;

  allPostsLoaded: boolean = false;

  loading: boolean = true;

  loadPage(): void { }

  loadNextPage(): void {
    this.page += 1;
    this.loadPage();
  }

}
