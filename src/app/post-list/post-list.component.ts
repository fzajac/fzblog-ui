import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Post } from 'src/model/post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['../app.component.scss', './post-list.component.scss']
})
export class PostListComponent {

  @Input({ required: true })
  posts: Array<Post> = [];

  @Input({ required: true })
  page: number = 0;

  @Input({ required: true })
  loading: boolean = true;

  @Input({ required: true })
  allPostsLoaded: boolean = false;

  @Output()
  onLoadPage: EventEmitter<any> = new EventEmitter();

  loadNextPage() {
    this.onLoadPage.emit();
  }

}
