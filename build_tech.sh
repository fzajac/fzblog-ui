#!/bin/sh
export APP_NAME=fz-blog-ui-tech
export BUILD_NAME=fz-blog-ui-tech_$(date -u +%s)
ng build -c tech
cd dist
mv $APP_NAME $BUILD_NAME
tar -cvzf $BUILD_NAME.tar.gz $BUILD_NAME
