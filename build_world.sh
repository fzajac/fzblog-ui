#!/bin/sh
export APP_NAME=fz-blog-ui-world
export BUILD_NAME=fz-blog-ui-world_$(date -u +%s)
ng build -c world
cd dist
mv $APP_NAME $BUILD_NAME
tar -cvzf $BUILD_NAME.tar.gz $BUILD_NAME
